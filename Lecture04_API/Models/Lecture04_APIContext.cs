﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Lecture04_API.Models
{
    public class Lecture04_APIContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public Lecture04_APIContext() : base("name=Lecture04_APIContext")
        {
        }

        public System.Data.Entity.DbSet<Lecture04_API.Models.Movie> Movies { get; set; }
    }
}
