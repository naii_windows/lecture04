﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lecture04_API.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }
    }
}