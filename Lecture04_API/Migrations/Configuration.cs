namespace Lecture04_API.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Lecture04_API.Models.Lecture04_APIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Lecture04_API.Models.Lecture04_APIContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Movies.AddOrUpdate(
                m => m.MovieID,
                new Models.Movie { Title = "Ready Player One", Director = "Steven Spielberg" },
                new Models.Movie { Title = "Star Wars", Director = "J.J. Abrams" },
                new Models.Movie { Title = "Star Trek", Director = "J.J. Abrams" }
                );
        }
    }
}
