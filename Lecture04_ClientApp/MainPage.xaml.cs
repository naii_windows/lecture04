﻿using Lecture04_ClientApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Lecture04_ClientApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ObservableCollection<Movie> lst;
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            HttpClient client = new HttpClient();
            var jsonResponse = await client.GetStringAsync(new Uri("http://localhost:60356/api/movies/"));
            var lst = JsonConvert.DeserializeObject<ObservableCollection<Movie>>(jsonResponse);
            lv.ItemsSource = lst;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var movie = new Movie() { Title = "New movie", Director = "With a maindirector" };
            var movieJson = JsonConvert.SerializeObject(movie);

            HttpClient client = new HttpClient();

            var res = await client.PostAsync("http://localhost:60356/api/movies/",
                new StringContent(movieJson, System.Text.Encoding.UTF8, "application/json"));

            if (res.StatusCode == System.Net.HttpStatusCode.Created)
            {
                //Add content to list to confirm adding
            }

        }
    }
}
